
import pool from "../database";
import { readFileSync } from 'fs'

const insert = readFileSync('../database/inserciones.sql').toString(); 
const dataSql = readFileSync('../database/db.sql').toString(); 

export const crearBaseDatos = ()=>{
    pool.query("use sia_unal",(err,result)=>{
        try {
            if (err) {
                pool.query(insert,(err,result)=>{
                    if (err) throw Error("Error base de datos")
                    return
                })
            }
            if (result) return
        } catch (error) {
            
        }
        
    })
}

export const insercionPrincipal = ()=>{
    pool.query("SELECT * from Rol",(err,result)=>{
        if (err) throw Error("Error base de datos")
        if (result.length > 0) return
        else{
            pool.query(insert,(err,result)=>{
                if (err) throw Error("Error base de datos")
                return
            })
        }
    })
}