import { SECRET } from "../config";
import pool from "../database";
import jwt from "jsonwebtoken";

export const verifyToken = async (req,res,netx)=>{
    try {
        const token = req.headers['x-access-token'];
        if (!token) return res.status(403).json({message:'Token no ingresado'})

        const decoder = jwt.verify(token,SECRET)
        req.id = decoder.id
        pool.query(
            "SELECT * from Estudiante WHERE cedula = ?",[decoder.id], (err, result) => {
                if (err) return res.status(501).json({err})
                if (result.lenght === 0) {
                    pool.query("SELECT id_rol FROM Funcionario where cedula = ?",[decoder.id],
                    (err,result)=>{
                        if(err) return res.status(501).json({err})
                        if (result.lenght===0) return res.status(404).json({"message":"Usuario no existe"})
                    })
                }
            })
        netx();
    } catch (error) {
        return res.status(401).json({message:"No autorizado"})
    }
}

export const verifyTokenEstudiante = async (req,res,netx)=>{
    try {
        const token = req.headers['x-access-token'];
        if (!token) return res.status(403).json({message:'Token no ingresado'})

        const decoder = jwt.verify(token,SECRET)
        req.id = decoder.id
        pool.query(
            "SELECT * from Estudiante WHERE cedula = ?",[decoder.id], (err, result) => {
                if (err) return res.status(501).json({err})
                if (result.lenght === 0) return res.status(404).json({"message":"Estudiante no existe"})
            })
        netx();
    } catch (error) {
        return res.status(401).json({message:"No autorizado"})
    }
}

export const verifyTokenFuncionario = async (req,res,netx)=>{
    try {
        const token = req.headers['x-access-token'];
        if (!token) return res.status(403).json({message:'Token no ingresado'})

        const decoder = jwt.verify(token,SECRET)
        req.id = decoder.id
        pool.query(
            "SELECT * from Funcionario WHERE cedula = ?",[decoder.id], (err, result) => {
                if (err) return res.status(501).json({err})
                if (result.lenght === 0) return res.status(404).json({"message":"Usuario no existe"})
            })
        netx();
    } catch (error) {
        return res.status(401).json({message:"No autorizado"})
    }
}

export const isAdminRole = (req,res,next)=>{
    pool.query(
        "SELECT b.role from Funcionario a inner join Rol b on a.id_rol = b.id_rol where cedula= ?",
            [req.id],(err,results) =>{
                if (err) res.status(501).json({err})
                if (results[0].role === 'Admin') netx()
                return res.status(403).json({message:"Admin requerido"})
        }
    )
}

export const isApoyoRole = (req,res,next)=>{
    pool.query(
        "SELECT b.role from Funcionario a inner join Rol b on a.id_rol = b.id_rol where cedula= ?",
            [req.id],(err,results) =>{
                if (err) res.status(501).json({err})
                if (results[0].role === 'Gestion y fomento socioeconimico') next()
                return res.status(403).json({message:"Gestion y fomento socioeconimico requerido"})
        }
    )
}

export const isRegistroRole = (req,res,next)=>{
    pool.query(
        "SELECT b.role from Funcionario a inner join Rol b on a.id_rol = b.id_rol where cedula= ?",
            [req.id],(err,results) =>{
                if (err) res.status(501).json({err})
                if (results[0].role === 'Registro y matricula') next()
                return res.status(403).json({message:"Registro y matricula requerido"})
        }
    )
}