import pool from "../database"

const estudianteCtrl = {}

estudianteCtrl.getEstudiante = (req, res) => {
    const estudiante = pool
        .query(`SELECT * FROM Estudiante WHERE cedula = ${res.params.id}`, (err, result) => {
            if (err) {
                res.json({ "err": err })
            }
            res.json({ estudiante: result })
        })
}
estudianteCtrl.editEstudiante = async (req, res) => {
    const estudiante = req.body;
    const id = req.params.id;
    pool.query("UPDATE Esudiante set ? where cedula = ?", [estudiante, id],
        (err, result) => {
            if (err) {
                res.json({ "err": err })
            }
            res.json({ "message": result })
        });
}
estudianteCtrl.getEstudiantes = async (req, res) => {
    const estudiante = pool
        .query(`SELECT * FROM Estudiante;`, (err, result) => {
            if (err) {
                res.json({ "err": err })
            }
            res.json({ estudidiantes: result })
        })
}

export default estudianteCtrl;