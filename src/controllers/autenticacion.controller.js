import pool from "../database"
import { SECRET } from "../config";

import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs"

const autenticacionCtrl = {}

autenticacionCtrl.signinEstudiante = (req, res) => {
    const user = req.body;
    pool.query(
        "SELECT cedula,constrasena FROM Estudiante where (correo_electronico = ? or usuario = ?)",
        [user.user, user.user],(err, result) => {
            if (err) res.json({ "err": err })
            if (result.length === 0) res.status(401).json({ "err": "EL CORREO NO EXISTE" })
            const password = result[0].constrasena
            if (bcrypt.compare(user.password,password)){
                const token = jwt.sign({ id: result[0].cedula }, 'secretKey')
                res.json({ token })
            }
            else res.status(403).json({"err":"USUARIO Y CONTRASEÑA INCORRECTA"})
            
        })

}


autenticacionCtrl.signupEstudiante = (req, res) => {
    const estudiante = req.body;
    pool.query(
        "SELECT * FROM Estudiante where (correo_electronico = ? or usuario = ?)",
        [estudiante.correo_electronico, estudiante.usuario],
        async (err, result) => {
            if (err) res.status(500).json({ "err": err })
            if (result.length > 0) res.status(402).json({ "err": {"msg":"EL ESTUDIANTE YA EXISTE","estudiante":result[0] }})
            else{
                const salt = await bcrypt.genSalt(10);
                const pass = await bcrypt.hash(estudiante.constrasena,salt)
                estudiante.constrasena = pass;
                let values_est = []
                Object.entries(estudiante).forEach((key,_) => {
                    values_est.push(key[1]);                    
                });
                pool.query(`INSERT INTO Estudiante VALUES (${"?,".repeat(values_est.length).slice(0, -1)})`,
                    values_est,(err,results)=>{
                        if (err) res.status(500).json({err})
                        if (result) res.status(200).json(results)
                    })
            }
        })

}


autenticacionCtrl.signinFuncionario = (req, res) => {
    const user = req.body;
    pool.query(
        "SELECT cedula,contrasena,id_rol FROM Funcionario where (correo = ? or usuario = ?)",
        [user.user, user.user],async (err, result) => {
            if (err) res.status(500).json({ "err": err })
            if (result.length === 0) res.status(401).json({ "err": "EL CORREO NO EXISTE" })
            else {
                const password = result[0].contrasena;
                console.log(user.password)
                const auth = await bcrypt.compare(user.password,password)
                if (!auth) return res.status(403).json({"err":"USUARIO Y CONTRASEÑA INCORRECTA"})
                const token = jwt.sign({ id: result[0].cedula }, SECRET,{
                    expiresIn:86400,})
                res.json({ token })
            }  
        })
}


autenticacionCtrl.signupFuncionario = (req, res) => {
    const funcionario = req.body;
    pool.query(
        "SELECT * FROM Funcionario where correo = ? or usuario = ?",
        [funcionario.correo, funcionario.usuario],
        async (err, result) => {
            if (err) res.status(500).json({ "err": err })
            if (result.length > 0) res.status(402).json({ "err": {"msg":"EL FUNCIONARIO YA EXISTE","funcionario":result[0] }})
            else{
                const salt = await bcrypt.genSalt(10);
                console.log(funcionario.contrasena,salt)
                const pass = await bcrypt.hash(funcionario.contrasena,salt)
                funcionario.contrasena = pass;
                // let values_est = []
                // Object.entries(funcionario).forEach((key,_) => {values_est.push(key[1]);});
                // pool.query(`INSERT INTO Funcionario VALUES (${"?,".repeat(values_est.length).slice(0, -1)})`,
                pool.query(`INSERT INTO Funcionario set ?`,[funcionario],(err,results)=>{
                        if (err) res.status(500).json({err})
                        if (result) res.status(200).json(results)
                    })
            }
        })
}

export default autenticacionCtrl;