import pool from "../database"

const apoyo_socioeconomicoCtrl = {}

apoyo_socioeconomicoCtrl.getApoyo_socioeconomico = async (req,res) => {
    pool.
        query(`SELECT * FROM Apoyo_socioeconomico WHERE id_apoyo_socioeconomico = ?`,[res.params.id],
            (err,result) => {
                if (err){
                    res.json({"err":err})
                }
                res.json({apoyo_socioeconomico:result})
            })
}

apoyo_socioeconomicoCtrl.createApoyo_socioeconomico = (req,res) => {
    const apoyo_socioeconomico = req.body;
    pool.query(
        `INSERT INTO Apoyo_socioeconomico (apoyo_socioeconomico) set ?`,
            [apoyo_socioeconomico], (err,result) => {
        if (err) return res.json({"err":err})
        if (result) return res.json({"message":result})
    });
    
}

apoyo_socioeconomicoCtrl.editApoyo_socioeconomico = async (req,res) => {
    const apoyo_socioeconomico = req.body;
    const id = req.params.id;
    pool.query("UPDATE Apoyo_socioeconomico set ? where id_apoyo = ?",[apoyo_socioeconomico,id], 
        (err,result) => {
            if (err){
                res.json({"err":err})
            } else{
                res.json({"message":result})
            }
            
        });
}
apoyo_socioeconomicoCtrl.getApoyo_socioeconomicos = async (req,res) => {
    pool
        .query(`SELECT * FROM Apoyo_socioeconomico`,(err,result) => {
            if (err) res.json({"err":err})
            else res.json({apoyos_socioeconomicos:result})
        })
}

apoyo_socioeconomicoCtrl.setApoyo_socioeconomico = (req,res) => {
    const body = req.body;
    pool.query(
        `SELECT id_estudiante from Estudiante WHERE cedula = ? and estado = 1`,
            [body.cedula], (err,result) => {
        if (err) res.status(500).json({"err":err})
        if (length(result) === 0) res.status(402).json({"message":result.message})
        pool.query("INSERT INTO Apoyo_socioeconomico_estudiante (id_estudiante,id_apoyo,created_by) VALUES (?,?,?)",
            [result[0].id_estudiante,body.id_apoyo,body.id_funcionario], (err,results)=> {
                if (err) res.status(500).json(err)
                if (results) res.status(200).json({"message":"Apoyo asignado"})
            })
        
    });
    
}

export default apoyo_socioeconomicoCtrl;