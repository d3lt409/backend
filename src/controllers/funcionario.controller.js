import pool from "../database"

const funcionarioCtrl = {}

funcionarioCtrl.getFuncionario = async (req,res) => {
    const funcionario = pool
        .query(`SELECT * FROM Funcionario WHERE cedula = ${res.params.id}`,(err,result) => {
            if (err){
                res.json({"err":err})
            }
            res.json({funcionario:result})
        })
}
    
funcionarioCtrl.editFuncionario = async (req,res) => {
    const funcionario = req.body;
    const id = req.params.id;
    pool.query("UPDATE Funcionario set ? where cedula = ?",[funcionario,id], 
        (err,result) => {
            if (err){
                res.json({"err":err})
            }
            res.json({"message":result})
        });
}
funcionarioCtrl.getFuncionarios = async (req,res) => {
    const funcionario = pool
        .query(`SELECT * FROM Funcionario;`,(err,result) => {
            if (err){
                res.json({"err":err})
            }
            res.json({funcionarios:result})
        })
}

export default funcionarioCtrl;