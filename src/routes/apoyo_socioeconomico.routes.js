import { Router } from "express";
const routerApoyo_socioeconomico = Router()

import apoyo_socioeconomicoCtrl from "../controllers/apoyo_socioeconomico.controller"
import { isApoyoRole, verifyTokenFuncionario } from "../middlewares/authjwt";

routerApoyo_socioeconomico.get("/", apoyo_socioeconomicoCtrl.getApoyo_socioeconomicos);
routerApoyo_socioeconomico.get("/:id",apoyo_socioeconomicoCtrl.getApoyo_socioeconomico);
routerApoyo_socioeconomico.post("/",[verifyTokenFuncionario,isApoyoRole], apoyo_socioeconomicoCtrl.createApoyo_socioeconomico);
routerApoyo_socioeconomico.put("/:id",[verifyTokenFuncionario,isApoyoRole],apoyo_socioeconomicoCtrl.editApoyo_socioeconomico);

export default routerApoyo_socioeconomico;