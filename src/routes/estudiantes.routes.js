import { Router } from "express";
const routerEstudiante = Router()

import estudianteCtrl from "../controllers/estudiate.controller"
import { isRegistroRole, verifyToken } from "../middlewares/authjwt";

routerEstudiante.get("/",verifyToken, estudianteCtrl.getEstudiantes);
routerEstudiante.get("/:id",verifyToken,estudianteCtrl.getEstudiante);
routerEstudiante.put("/:id",verifyToken,estudianteCtrl.editEstudiante);

export default routerEstudiante;