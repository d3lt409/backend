import { Router } from "express";
const routerFuncionario = Router()

import funcionarioCtrl from "../controllers/funcionario.controller"
import { verifyTokenFuncionario } from "../middlewares/authjwt";

routerFuncionario.get("/",verifyTokenFuncionario, funcionarioCtrl.getFuncionarios);
routerFuncionario.get("/:id",verifyTokenFuncionario,funcionarioCtrl.getFuncionario);
routerFuncionario.put("/:id",verifyTokenFuncionario,funcionarioCtrl.editFuncionario);

export default routerFuncionario;