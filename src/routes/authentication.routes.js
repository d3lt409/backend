import { Router } from "express";
const routerAutenticaion = Router()

import autenticacionCtrl from "../controllers/autenticacion.controller"
import { isAdminRole, isRegistroRole, verifyTokenEstudiante, verifyTokenFuncionario } from "../middlewares/authjwt";

routerAutenticaion.post("/signinEstudiante", autenticacionCtrl.signinEstudiante);
routerAutenticaion.post("/signupEstudiante",[verifyTokenFuncionario,isRegistroRole], autenticacionCtrl.signupEstudiante);
routerAutenticaion.post("/signinFuncionario", autenticacionCtrl.signinFuncionario);
routerAutenticaion.post("/signupFuncionario",[verifyTokenFuncionario,isAdminRole], autenticacionCtrl.signupFuncionario);


export default routerAutenticaion;