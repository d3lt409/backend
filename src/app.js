import express from "express";
import morgan from "morgan";
import estudianteRoutes from "./routes/estudiantes.routes"
import funcionarioRoutes from "./routes/funcionario.routes"
import authenticationRoutes from "./routes/authentication.routes"
import apoyo_socioeconomicoRoutes from "./routes/apoyo_socioeconomico.routes"
import cors from 'cors'


const app = express()

app.set('port',process.env.PORT || 3000)

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

//Variables globales
app.use((req,res,next)=>{
    next();
})


//Routes 
app.use("/api/estudiantes",estudianteRoutes)
app.use("/api/funcionarios",funcionarioRoutes)
app.use("/api/apoyo_socioeconomico",apoyo_socioeconomicoRoutes)
app.use("/api/auth",authenticationRoutes)

export default app