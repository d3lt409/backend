import mysql from "mysql";
import { promisify } from "util"
import { database } from "./config"

const pool = mysql.createPool(database);

pool.getConnection((err,conn)=>{
    if (err){
        if (err.code === 'PROTOCOL_CONNECTION_LOST'){
            console.error('BASE DE DATOS FUE CERRADA');
        } else if (err.code === 'ER_CON_COUNT_ERROR'){
            console.error('BASE DE DATOS TIENE MUCHAS CONEXIONES');
        } else if (err.code === 'ECONNREFUSED'){
            console.error("BASE DE DATOS FUE RECHAZADA");
        }else{
            console.error(err)
        }
    }if (conn){
        conn.release();
        console.log('BASE DE DATOS está conectada')
        return
    }
})

pool.query = promisify(pool.query);

export default pool;