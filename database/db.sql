CREATE DATABASE sia_unal;

USE sia_unal;

CREATE TABLE `Rol`(
    `id_rol` int NOT NULL AUTO_INCREMENT COMMENT 'Identificador de llave primaria',
    `rol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nombre del rol que tendrán las personas',
    `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Descripcion del rol que tendrán las personas',
    PRIMARY KEY (`id_rol`)
);

CREATE TABLE `Estado`(
    `id_estado` int  NOT NULL AUTO_INCREMENT COMMENT 'Identificador de llave primaria',
    `estado` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nombre del estado que tendrán las personas',
    `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Descripcion del estado que tendrán las personas',
    PRIMARY KEY (`id_estado`)
);

CREATE TABLE `Departamento`(
    `id_departamento` int not null AUTO_INCREMENT,
    `departamento` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    PRIMARY KEY (`id_departamento`)
);

CREATE TABLE `Municipio`(
    `id_municipio` int not null AUTO_INCREMENT,
    `municipio` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `id_departamento` int NOT NULL,
    PRIMARY KEY (`id_municipio`),
    CONSTRAINT `Ciudad_ibfk_1` FOREIGN KEY (`id_departamento`) REFERENCES `Departamento` (`id_departamento`)
);

CREATE TABLE `Funcionario`(
    `cedula` INT NOT NULL UNIQUE COMMENT 'Identificador de llave primaria',
    `primer_nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nombre del estudiante',
    `segundo_nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Segundo nombre del estudiante',
    `primer_apellido` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Primer apellido del estudiante',
    `segundo_apellido` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'segundo apellido del estudiante',
    `fecha_nacimiento` DATE NOT NULL,
    `usuario` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE COMMENT 'Usuario del estudiante',
    `correo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE COMMENT 'correo del estudiante',
    `contrasena` varchar(50) NOT NULL COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Contraseña del estudiante',
    `id_estado` int DEFAULT 1,
    `id_rol` int NOT NULL,
    PRIMARY KEY (`cedula`),
    CONSTRAINT `person_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `Rol` (`id_rol`),
    CONSTRAINT `person_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `Estado` (`id_estado`)
);

CREATE TABLE `Estudiante` (
    `cedula` int NOT NULL UNIQUE COMMENT 'Cedula del estudiant, clave clave primaria',
    `primer_nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nombre del estudiante',
    `segundo_nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Segundo nombre del estudiante',
    `primer_apellido` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Primer apellido del estudiante',
    `segundo_apellido` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'segundo apellido del estudiante',
	`fecha_nacimiento` DATE NOT NULL,
    `telefono` int NOT NULL COMMENT 'Telefono del estudiante',
	`correo_electronico` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE COMMENT 'Correo personal del estudiante',
	`situacion_militar` boolean DEFAULT 0 COMMENT 'Situacion militar activa o inactiva',
	`etnia` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Etnia del estudiante',
	`direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Direccion de residencia del estudiante',
	`pais` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Pais de nacimiento del estudiante',
	`id_departamento` int NOT NULL COMMENT 'Departamento de nacimiento del estudiante',
	`id_municipio` int NOT NULL COMMENT 'Municipio de nacimiento del estudiante',
	`codigo_postal` int NOT NULL COMMENT 'Codigo postal del estudiante',
	`telefono_fijo` int DEFAULT NULL COMMENT 'Telefono fijo del estudiante',
	`estrato` tinyint NOT NULL COMMENT 'Estrato del estudiante',
	`acta_de_grado` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE COMMENT 'Numero del acta de grado del estudiante',
	`resultado_icfes` int NOT NULL COMMENT 'Puntaje prueba ICFES del estudiante',
	`nombre_responsable` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nombre del responsable del estudiante',
	`pbm` int DEFAULT NULL COMMENT 'Puntaje Basico de Matricula del estudiante',
	`nombre_eps` varchar(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT 'Nombre de la Entidad Promotora de Salud del estudiante',
	`ingresos_responsable` int NOT NULL COMMENT 'Total de ingresos del responsable del estudiante',
    `usuario` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE COMMENT 'Usuario del estudiante',
    `contrasena` varchar(50) NOT NULL COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Contraseña del estudiante',
    `created_at` timestamp DEFAULT current_timestamp,
    `created_by` int NOT NULL DEFAULT 1111 COMMENT 'Id del Usuario que crea el registro',
    `updated_by` int NOT NULL DEFAULT 1111,
    `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
    `id_estado` int DEFAULT 1,
    PRIMARY KEY (`id_estudiante`),
    CONSTRAINT `estudiante_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `Funcionario` (`id_funcionario`),
    CONSTRAINT `estudiante_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `Funcionario` (`id_funcionario`),
    CONSTRAINT `estudiante_ibfk_3` FOREIGN KEY (`id_estado`) REFERENCES `Estado` (`id_estado`),
    CONSTRAINT `estudiante_ibfk_4` FOREIGN KEY (`id_departamento`) REFERENCES `Departamento` (`id_departamento`),
    CONSTRAINT `estudiante_ibfk_5` FOREIGN KEY (`id_municipio`) REFERENCES `Municipio` (`id_municipio`)
);

CREATE TABLE `Apoyo_socioeconomico`(
    `id_apoyo` int NOT NULL AUTO_INCREMENT,
    `apoyo_socioeconomico` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `estado` boolean NOT NULL DEFAULT 1,
    PRIMARY KEY (`id_apoyo`)
);

CREATE TABLE `Apoyo_socioeconomico_estudiante`(
    `id_apoyo` int NOT NULL,
    `id_estudiante` int NOT NULL,
    `estado` boolean NOT NULL DEFAULT 1,
    `created_at` timestamp DEFAULT current_timestamp,
    `created_by` int DEFAULT NULL COMMENT 'Id del Usuario que crea el registro',
    PRIMARY KEY (`id_apoyo`,`id_estudiante`),
    CONSTRAINT `Apoyo_socioeconomico_asignado_ibfk_1` FOREIGN KEY (`id_apoyo`) REFERENCES `Apoyo_socioeconomico` (`id_apoyo`),
    CONSTRAINT `Apoyo_socioeconomico_asignado_ibfk_2` FOREIGN KEY (`id_estudiante`) REFERENCES `Estudiante` (`id_estudiante`),
    CONSTRAINT `Apoyo_socioeconomico_asignado_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `Funcionario` (`id_funcionario`)
);
 